

<?php



	//distrito passado por GET
		$distrito=$_GET['distrito'];
		
		//array associativo
		$concelhos['Aveiro']			=array('�gueda','Albergaria-a-Velha','Anadia','Arouca','Aveiro','Castelo de Paiva','Espinho','Estarreja','Santa Maria da Feira','�lhavo','Mealhad','Murtosa','Oliveira de Azem�is','Oliveira do Bairro','Ovar','S�o Jo�o da Madeira','Sever do Vouga','Vagos','Vale de Cambra');
		$concelhos['Beja']				=array('Aljustrel','Almod�var','Alvito','Barrancos','Beja','Castro Verde','Cuba','Ferreira do Alentejo','M�rtola','Moura','Odemira','Ourique','Serpa','Vidigueira');
		$concelhos['Braga']				=array('Amares','Barcelos','Braga','Cabeceiras de Basto','Celorico de Basto','Esposende','Fafe','Guimar�es','P�voa de Lanhoso','Terras de Bouro','Vieira do Minho','Vila Nova de Famalic�o','Vila Verde','Vizela');
		$concelhos['Bragan�a']			=array('Alfandega da F�','Bragan�a','Carrazeda de Ansi�es','Freixo Espada � Cinta','Macedo de Cavaleiros','Miranda do Douro','Mirandela','Mogadouro','Torre de Moncorvo','Vila Flor','Vimioso','Vinhais');
		$concelhos['Castelo Branco']	=array('Belmonte','Castelo Branco','Covilh�','Fund�o','Idanha-a-Nova','Oleiros','Penamacor','Proen�a-a-Nova','Sert�','Vila de Rei','Vila Velha de Rod�o');
		$concelhos['Coimbra']			=array('Arganil','Cantanhede','Coimbra','Condeixa-a-Nova','Figueira da Foz','G�is','Lous�','Mira','Miranda do Corvo','Montemor-o-Velho','Oliveira do Hospital','Pampilhosa da Serra','Penacova','Penela','Soure','T�bua','Vila Nova de Poiares');
		$concelhos['�vora']				=array('Alandroal','Arraiolos','Borba','Estremoz','�vora','Montemor-o-Novo','Mora','Mour�o','Portel','Redondo','Reguengos de Monsaraz','Vendas Novas','Viana do Alentejo','Vila Vi�osa');
		$concelhos['Faro']				=array('Albufeira','Alcoutim','Aljezur','Castro Marim','Faro','Lagoa (Algarve)','Lagos','Loul�','Monchique','Olh�o','Portim�o','S�o Br�s de Alportel','Silves','Tavira','Vila do Bispo','Vila Real de Santo Ant�nio');
		$concelhos['Guarda']			=array('Aguiar da Beira','Almeida','Celorico da Beira','Figueira de Castelo Rodrigo','Fornos de Algodres','Gouveia','Guarda','Manteigas','Meda','Pinhel','Sabugal','Seia','Trancoso','Vila Nova de Foz C�a');
		$concelhos['Leiria']			=array('Alcoba�a','Alvai�zere','Ansi�o','Batalha','Bombarral','Caldas da Rainha','Castanheira de P�ra','Figueir� dos Vinhos','Leiria','Marinha Grande','Nazar�','�bidos','Pedr�g�o Grande','Peniche','Pombal','Porto de M�s','Alenquer');
		$concelhos['Lisboa']			=array('Arruda dos Vinhos','Azambuja','Cadaval','Cascais','Lisboa','Loures','Lourinh�','Mafra','Oeiras','Sintra','Sobral de Monte Agra�o','Torres Vedras','Vila Franca de Xira','Amadora','Odivelas');
		$concelhos['Portalegre']		=array('Alter do Ch�o','Arronches','Avis','Campo Maior','Castelo de Vide','Crato','Elvas','Fronteira','Gavi�o','Marv�o','Monforte','Nisa','Ponte de Sor','Portalegre','Sousel');
		$concelhos['Porto']				=array('Amarante','Bai�o','Felgueiras','Gondomar','Lousada','Maia','Marco de Canaveses','Matosinhos','Pa�os de Ferreira','Paredes','Penafiel','Porto','P�voa de Varzim','Santo Tirso','Valongo','Vila do Conde','Vila Nova de Gaia','Trofa');
		$concelhos['Santar�m']			=array('Abrantes','Alcanena','Almeirim','Alpiar�a','Benavente','Cartaxo','Chamusca','Const�ncia','Coruche','Entroncamento','Ferreira do Z�zere','Goleg�','Ma��o','Rio Maior','Salvaterra de Magos','Santar�m','Sardoal','Tomar','Torres Novas','Vila Nova da Barquinha','Our�m15','Alc�cer do Sal');
		$concelhos['Set�bal']			=array('Alcochete','Almada','Barreiro','Gr�ndola','Moita','Montijo','Palmela','Santiago do Cac�m','Seixal','Sesimbra','Set�bal','Sines');
		$concelhos['Viana do Castelo']	=array('Arcos de Valdevez','Caminha','Melga�o','Mon��o','Paredes de Coura','Ponte da Barca','Ponte de Lima','Valen�a','Viana do Castelo','Vila Nova de Cerveira');
		$concelhos['Vila Real']			=array('Alij�','Boticas','Chaves','Mes�o Frio','Mondim de Basto','Montalegre','Mur�a','Peso da R�gua','Ribeira de Pena','Sabrosa','Santa Marta de Penagui�o','Valpa�os','Vila Pouca de Aguiar','Vila Real');
		$concelhos['Viseu']				=array('Armamar','Carregal do Sal','Castro Daire','Cinf�es','Lamego','Mangualde','Moimenta da Beira','Mort�gua','Nelas','Oliveira de Frades','Penalva do Castelo','Penedono','Resende','Santa Comba D�o','S�o Jo�o da Pesqueira','S�o Pedro do Sul','S�t�o','Sernancelhe','Tabua�o','Tarouca','Tondela','Vila Nova de Paiva','Viseu','Vouzela');
		$concelhos['Ilha da Madeira']	=array('Calheta (Madeira)','C�mara de Lobos','Funchal','Machico','Ponta do Sol','Porto Moniz','Ribeira Brava','Santa Cruz','Santana','S�o Vicente');
		$concelhos['Ilha de Porto Santo']=array('Porto Santo');
		$concelhos['Ilha de Santa Maria']=array('Vila do Porto');
		$concelhos['Ilha de S�o Miguel']=array('Lagoa (S�o Miguel)','Nordeste','Ponta Delgada','Povoa��o','Ribeira Grande','Vila Franca do Campo');
		$concelhos['Ilha Terceira']		=array('Angra do Hero�smo','Praia da Vit�ria');
		$concelhos['Ilha da Graciosa']	=array('Santa Cruz da Graciosa');
		$concelhos['Ilha de S�o Jorge']	=array('Calheta (S�o Jorge)','Velas');
		$concelhos['Ilha do Pico']		=array('Lajes do Pico','Madalena','S�o Roque do Pico');
		$concelhos['Ilha do Faial']		=array('Horta');
		$concelhos['Ilha das Flores']	=array('Lajes das Flores','Santa Cruz das Flores');
		$concelhos['Ilha do Corvo']		=array('Corvo');
		if($distrito!="")
		{
			echo "<select style='width: 160px'>";
		}
		else
		{
			echo "<select disabled style='width: 160px'>";
		}
		
		foreach($concelhos[$distrito] as $concelho)
			echo '<option value="' . $concelho . '">' . $concelho . '</option>';
		echo '</select>';

?>
